
var player : GameObject;
var speed : float  = 5;
var rotationSpeed : float = 5;

var bullet : GameObject;
var explosion : GameObject;

private var health : int;
private var shootTimer : float;

function Start()
{
	rotationSpeed = 3;
	player = GameObject.FindWithTag("Player");
	health = 100;
	shootTimer = 0;
}

private var counter : float = 5;
private var target : Vector3;

function Update () 
{		
	if (counter > 8 || Vector3.Distance(transform.position,target) < 10) {
		MakeTarget();		
		counter = 0;
	}
	counter += Time.deltaTime;
	
	MoveToTarget();
	
	if (shootTimer > 3)
	{
		Fire();
		shootTimer = 0;
	}
	
	shootTimer += Time.deltaTime;
}

function MakeTarget()
{
	var direction : Vector3 = (player.transform.position - transform.position);
	var distance : float = direction.magnitude;
	
	Debug.Log(distance);
		
	if (distance > 30)
	{
		target = player.transform.position;
	} 
	else 
	{
		var rx : int = Random.Range(-60, 60);
		var ry : int = Random.Range(-60, 60);
				
		target = player.transform.position + 
			player.transform.right * rx;
			player.transform.forward * ry;
	}
}
function MoveToTarget()
{	
	var direction : Vector3 = target - transform.position;
	transform.rotation = 
		Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
	
	transform.Translate(0, 0, speed * Time.deltaTime);
}

function Fire()
{
	var v = transform.position + transform.forward*3 + Vector3(0,1,0);
	
	var bulletClone : GameObject = Instantiate(bullet, v, Quaternion.identity);
	bulletClone.rigidbody.velocity = transform.forward * 30;
}

function OnCollisionEnter(collision : Collision)
{
	if (collision.gameObject.tag == "Bullet")
	{		
		Instantiate(explosion, transform.position, Quaternion.identity);
		rigidbody.AddForce(Vector3(0,300,0));
		//rigidbody.angularVelocity = Vector3(10, 0, 40);
		Destroy(gameObject, 1);
	}
}