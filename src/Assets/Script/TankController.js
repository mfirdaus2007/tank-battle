
// public變數可以在Inspector裡面觀看及修改
// 坦克的前進速度 (每秒5公尺)
public var speed : float = 5.0f;

// 坦克的轉彎速度 (每秒20度)
public var rotationSpeed : float = 20f;

function Update ()
{
	// 控制前進後退
	var move : float = Input.GetAxis("Vertical") * speed * Time.deltaTime;
	transform.Translate(0, 0, move);
	
	// 控制左右轉
	var rotation : float = Input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime;
	transform.Rotate(0, rotation, 0);
	
	if (Input.GetKeyDown(KeyCode.X))
	{
		if (health > 0)
			health -= 10;
	}	
}

var healthBar : Texture;
var health : int;

function Start()
{
	health = 100;
}

function OnGUI()
{	
	GUI.Box(Rect(10, 10, 110, 40), "");
	GUI.DrawTexture(Rect(15, 15, health, 30), healthBar, ScaleMode.ScaleAndCrop);
}