
public var enemyTankPrefab : GameObject;
public var enemyCount : int;
public var enemyCreateTimer : float;

function Start() 
{
	enemyCreateTimer = 8;
}

function Update () {

	if (enemyCount < 5 && enemyCreateTimer > 10)
	{
		Instantiate(enemyTankPrefab, transform.position, Quaternion.identity);
		enemyCreateTimer = 0;
	}
	enemyCreateTimer += Time.deltaTime;
}