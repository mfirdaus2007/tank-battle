
public var bullet : GameObject;

function Update () {
		
	if (Input.GetButtonDown("Fire1")) 
	{		
		particleEmitter.Emit();		
		Fire();
	}	
}

function Fire()
{
	// Instantiate : Clone a new bullet
	var bulletClone : GameObject = Instantiate(bullet, transform.position, Quaternion.identity);
	bulletClone.rigidbody.velocity = transform.forward * 100;
}